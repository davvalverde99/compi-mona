import re
import token

#La clase Scanner se encarga de clasificar la entrada de los tokens respectivos y reporta errores.

class Scanner:

  def __init__(self,linea):
    """Constructor que crea un objeto de tipo Scanner"""
    linea = linea.replace('\t', '') #Elimina los tabs de la entrada
    self.linea = linea.replace('\n', ' \n ') #Separar los \n con espacios
    self.linea = self.linea.split(' ') #Hace lista a base de separar strings por espacios
    self.linea = list(filter(None, self.linea)) #Quitar strings nulos
    self.tokenLista = []
    self.tokenError = []
    self.tokenActual = 0
    self.lineaActual = 0
    self.palabraActual = 0


  def scanCadena(self,simbolo):
      """Funcion que escanea tokens con un simbolo de inicio 
      y un simbolo de final, como las cadenas y los comentarios."""
      finalizar = False
      actual = ""
      if self.tokenActual < len(self.linea) and self.linea[self.tokenActual][0] == simbolo:
         if simbolo == "₡":
           self.tokenLista.append(token.Token("Comentario",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1))) #Concatenar la entidad como parte del token Cadena
           error = "Comentario"
         else:
           self.tokenLista.append(token.Token("Cadena",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1))) #Concatenar la entidad como parte del token Cadena
           error = "Cadena"
         actual = self.linea[self.tokenActual]
         posicion = 0

         if actual[posicion] == simbolo: #Si el primer token empieza con el simbolo indicado en la entrada
            posicion = posicion + 1

            while self.tokenActual < len(self.linea) and finalizar == False:

                while posicion < len(actual) and actual[posicion] != simbolo: #Recorrer hasta encontrar otro simbolo igual al de la entrada
                      posicion = posicion + 1

                if posicion >= len(actual): #Si se terminó de recorrer un token sin encontrar el simbolo ingresado
                   self.tokenActual = self.tokenActual +1

                   if self.tokenActual < len(self.linea):
                       self.tokenLista[len(self.tokenLista) -1].appendValor(self.linea[self.tokenActual]) #Concatenar la entidad como parte del token Cadena o Comentario
                       actual = self.linea[self.tokenActual]
                       posicion = 0

                if self.tokenActual < len(self.linea):

                    if actual[posicion] == simbolo: #Si se encontró el simbolo

                       if posicion == len(actual) -1: #Si el segundo simbolo está al final del token respectivo
                           finalizar = True

                       break

         if finalizar == False: #Error si se llego al final de la lista de tokens y no se encontro el simbolo del token respectivo
             self.tokenLista.pop() #Eliminar token incompleto
             self.tokenError.append("Error en la palabra numero " + str(self.palabraActual+1)+", en la linea numero "+ str(self.lineaActual+1))
             self.tokenError.append("Error: se llego al final de la linea y no se encontro el simbolo "  + simbolo + " " + "(el segundo " + simbolo + " no finaliza correctamente el token " + error + " )")
             return "Rechazado" 

         self.tokenActual = self.tokenActual +1
         self.palabraActual = self.palabraActual+1
         return "Aceptado" 


  def scanTokens(self):
    """Función que escanea a todos los tokens de la gramática"""
    palabras_reservadas = ['mae','mona','diay','upee','sarpe','ni','modo','sobrada','&','|','+','-','*','/','<','>','<=','>=','==','!=','=','(',')',',',';','{','}']
    funciones_predefinidas = ['imprimir()','largo(','indice(','concatenar(','solicitar()','convertir(']
    
    while self.tokenActual < len(self.linea) and self.tokenError == []:

      cadena = self.scanCadena('\"') #Se buscan tokens que sean cadenas de caracteres
      comentario = self.scanCadena('₡') #Se buscan tokens que sean comentarios

      #Si no es Cadena o Comentario
      if cadena == "Rechazado" and comentario == "Rechazado": 
          continue

      #Revisa si el token es un operador logico
      if re.match("[&|\|]",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                    
        self.tokenLista.append(token.Token("OperadorLogico",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si es token operador
      elif re.match("[+|-|*|/]",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:
        self.tokenLista.append(token.Token("Operador",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si es token numero
      elif re.match("[0-9]+",self.linea[self.tokenActual]) != None: 
        self.tokenLista.append(token.Token("Numero",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si es una funcion predefinida
      elif self.linea[self.tokenActual] in funciones_predefinidas:
        self.tokenLista.append(token.Token("FuncionPredefinida",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una variable global
      elif re.match("sobrada",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas: 
        self.tokenLista.append(token.Token("VariableGlobal",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una condicion sino
      elif re.match("ni",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:             #Revisa si el token es un Else
            if  re.match("modo",self.linea[self.tokenActual+1]) != None and self.linea[self.tokenActual] in palabras_reservadas:
               self.tokenLista.append(token.Token("Sino",str(self.linea[self.tokenActual])+" " +str(self.linea[self.tokenActual+1]),str(self.lineaActual+1),str(self.palabraActual+1)))
               self.tokenActual = self.tokenActual+1
               self.palabraActual = self.palabraActual+1
      #Revisa si el token es un identificador
      elif re.match("^[a-z-A-Z-0-9-_]+$",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual]  not in palabras_reservadas :
        self.tokenLista.append(token.Token("Identificador",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))                                               
      #Revisa si el token es un cambio de linea
      elif re.match("\n",self.linea[self.tokenActual]) != None:                        
        self.palabraActual = -1
        self.lineaActual+=1
      #Revisa si el token es un parentesis
      elif re.match("[()]",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                    
        self.tokenLista.append(token.Token("Parentesis",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una Coma
      elif re.match(",",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                 
        self.tokenLista.append(token.Token("Coma",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es un corchete
      elif re.match("[{}]",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                    
        self.tokenLista.append(token.Token("Corchete",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es un punto y coma
      elif re.match(";",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                
        self.tokenLista.append(token.Token("PuntoyComa",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
       #Revisa si el token es una Comparador
      elif re.match("<|>|<=|>=|==|!=",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:        
        self.tokenLista.append(token.Token("Comparador",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una Asignacion
      elif re.match("[=]",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                   
        self.tokenLista.append(token.Token("Asignacion",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
       #Revisa si el token es una condicion si
      elif re.match("diay",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:            
        self.tokenLista.append(token.Token("Si",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es un retorno
      elif re.match("sarpe",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:             
        self.tokenLista.append(token.Token("Retorno",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una repeticion
      elif re.match("upee",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:                
        self.tokenLista.append(token.Token("Repeticion",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      #Revisa si el token es una funcion
      elif re.match("mae",self.linea[self.tokenActual]) != None and self.linea[self.tokenActual] in palabras_reservadas:
         #Verifica si se trata de la funcion main
        if  re.match("mona",self.linea[self.tokenActual+1]) != None and self.linea[self.tokenActual] in palabras_reservadas:
               self.tokenLista.append(token.Token("Main",str(self.linea[self.tokenActual])+" " +str(self.linea[self.tokenActual+1]),str(self.lineaActual+1),str(self.palabraActual+1)))
               self.tokenActual = self.tokenActual+1
               self.palabraActual = self.palabraActual+1
        #Verifica si se trata de una funcion diferente de la funcion main
        else:
          self.tokenLista.append(token.Token("Funcion",str(self.linea[self.tokenActual]),str(self.lineaActual+1),str(self.palabraActual+1)))
      else:
        #Si el token no es encontrado se agrega el error a la lista de errores
        self.tokenError.append("Error en la palabra numero " + str(self.palabraActual+1) + ", linea numero "+str(self.lineaActual+1)+" : Token "+ str(self.linea[self.tokenActual]) + " no identificado")
      self.tokenActual = self.tokenActual+1
      self.palabraActual = self.palabraActual+1

