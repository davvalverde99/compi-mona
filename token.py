#La clase Token se encarga de guardar los datos necesarios que necesita los tokens.

class Token:
  tipo = ""
  valor = ""
  linea = ""
  palabra = ""
  
  def __init__(self,tipo,valor,linea,palabra):
    """Constructor del objeto de tipo Token"""
    self.tipo = tipo
    self.valor = valor
    self.linea = linea
    self.palabra = palabra
  
  def getTipo(self):
    """Función que obtiene el tipo del token"""
    return self.tipo
  
  def getValor(self): 
    """Función que obtiene el valor del token"""
    return self.valor

  def getLinea(self):
    """Función donde se obtiene la linea en la que se encuentra el token"""
    return self.linea

  def getPalabra(self):
    """Funcion que obtiene la palabra en la cual se encuentra el token"""
    return self.palabra

  def appendValor(self,valor):
    """Función de agregacion de contenido a la variable valor"""
    self.valor = self.valor +" "+ valor

  def printToken(self): 
    """Función que imprime el token con el formato de salida"""
    print(repr("<"+self.tipo+","+self.valor+","+"linea: "+self.linea+","+"palabra: "+self.palabra+">"))

		