class MicoBrujo:

    def __init__(self,bloque):
        """Constructor del objeto MicoBrujo"""
        self.bloque = [bloque]
   

    def __str__(self,nivel=0):
        """Metodo para crear el arbol de tipo MicoBrujo"""
        arbol = "\t"*nivel+"MicoBrujo\n"
        if self.bloque != [False]:
            for contenido in self.bloque:
                arbol+= contenido.__str__(nivel+1)+ "\n"
        return arbol

class Funcion:

    def __init__(self,comentario,identificador,parametros,instrucciones):
        """Constructor para crear el objeto tipo Funcion"""
        self.comentario = comentario
        self.identificador = identificador
        self.parametros = parametros
        self.instrucciones = instrucciones

    def __str__(self,nivel):
        """Metodo para crear el arbol de tipo Funcion"""
        funcion = "\t"*nivel+"Funcion\n"
        
        if self.comentario != False:
            funcion += self.comentario.__str__(nivel+1)+ "\n"

        funcion += self.identificador.__str__(nivel+1)+ "\n"

        for parametro in self.parametros:
            funcion+= parametro.__str__(nivel+1)+ "\n"

        for instruccion in self.instrucciones:
            funcion+= instruccion.__str__(nivel+1)+ "\n"

        return funcion

class Instrucciones:

    def __init__(self,bloque):
        """Constructor para crear el objeto de tipo Instrucciones"""
        self.bloque = [bloque]

    def __str__(self,nivel):
        """Metodo para imprimir el arbol de tipo Instrucciones"""
        instrucciones = "\t"*nivel+"Instrucciones\n"

        for instruccion in self.bloque:
            instrucciones += instruccion.__str__(nivel+1)+ "\n"

        return instrucciones

class Comentario:

    def __init__(self,contenido):
        """Constructor para crear el objeto de tipo Comentario"""
        self.contenido = contenido
    
    def __str__(self,nivel):
        """Metodo para imprimir el arbol de tipo comentario"""
        comentario = "\t"*nivel+"Comentario - " + self.contenido + "\n"
        
        return comentario

class Identificador:

    def __init__(self,contenido):
        """Constructor para crear el objeto de tipo Identificador"""
        self.contenido = contenido 

    def __str__(self,nivel):
        """Metodo para imprimir el arbol de tipo Identificador"""
        identificador = "\t"*nivel+"Identificador - " + self.contenido + "\n"
        
        return identificador

class Repeticion:

    def __init__(self,condicion,instrucciones):
        """Constructor para crear el objeto de tipo Repetcion"""
        self.condicion = condicion
        self.instrucciones = instrucciones

    def __str__(self,nivel):
        """Metodo para impirmir el arbol de tipo Repetcion"""
        repeticion = "\t"*nivel+"Repeticion\n"

        repeticion += self.condicion.__str__(nivel+1)+ "\n"

        for instruccion in self.instrucciones:
            repeticion += instruccion.__str__(nivel+1)+ "\n"

        return repeticion

class Bifurcacion:

    def __init__(self,si,sino):
        """Constructor del objeto de tipo Bifurcacion"""
        self.si = si
        self.sino = sino

    def __str__(self,nivel):
        """Metodo para imprimir el arbol de tipo Bifurcacion"""
        bifurcacion = "\t"*nivel+"Bifurcacion\n"

        bifurcacion += self.si.__str__(nivel+1)+ "\n"

        if self.sino != False:
            bifurcacion += self.sino.__str__(nivel+1)+ "\n"

        return bifurcacion

class Si:

    def __init__(self,condicion,instrucciones):
        """Constructor del objeto de tipo Si"""
        self.condicion = condicion
        self.instrucciones = instrucciones    

    def __str__(self,nivel):
        """Metodo para imprimir el nodo del arbol de tipo Si"""
        si = "\t"*nivel+"Si\n"

        si += self.condicion.__str__(nivel+1)+ "\n"

        for instruccion in self.instrucciones:
            si += instruccion.__str__(nivel+1)+ "\n"

        return si

class Sino:

    def __init__(self,instrucciones):
        """Constructor del objeto de tipo Sino"""
        self.instrucciones = instrucciones

    def __str__(self,nivel):
        """Metodo para imprimir el nodo del arbol de tipo Sino"""
        sino = "\t"*nivel+"Sino\n"

        for instruccion in self.instrucciones:
            sino += instruccion.__str__(nivel+1)+ "\n"

        return sino

class Operador:

    def __init__(self,simbolo):
        """Constructor para crear el objeto Operador"""
        self.simbolo = simbolo

    def __str__(self,nivel):
        """Metodo para imprimir el nodo del arbol de tipo Operador"""
        operador = "\t"*nivel+"Operador - " + self.simbolo + "\n"

        return operador

class Condicion:

    def __init__(self,comp1,comp2):
        """Constructor para crear el objeto Condicion"""
        self.comp1 = comp1
        self.comp2 = comp2

    def __str__(self,nivel):
        """Metodo para imprimir el nodo del arbol de tipo Condicion"""
        condicion = "\t"*nivel+"Condicion\n"

        condicion+=self.comp1.__str__(nivel+1)+ "\n"

        if self.comp2 != False:
            condicion+=self.comp2.__str__(nivel+1)+ "\n"

        return condicion


class Comparacion:
    
    def __init__(self,parametro1, comparador, parametro2):
        """Constructor para crear el objeto de tipo Comparacion"""
        self.parametro1 = parametro1
        self.comparador = comparador
        self.parametro2 = parametro2

    def __str__(self,nivel):
        """Metodo para imprimir el nodo del arbol de tipo Comparacion"""
        comparacion = "\t"*nivel+"Comparacion\n"

        comparacion+=self.parametro1.__str__(nivel+1)+ "\n"
        comparacion+=self.comparador.__str__(nivel+1)+ "\n"
        comparacion+=self.parametro2.__str__(nivel+1)+ "\n"

        return comparacion

class Comparador:

    def __init__(self,simbolo):
        """Constructor para crear el objeto de tipo Comparador"""
        self.simbolo = simbolo

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Comparador"""
      comparador = "\t"*nivel+"Comparador\n"

      comparador+=self.simbolo.__str__(nivel+1)+ "\n"

      return comparador

class Parametro:

    def __init__(self,contenido):
      """Constructor para crear el objeto de tipo Parametro"""
      self.contenido = contenido

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Parametro"""
      parametro = "\t"*nivel+"Parametro\n"
      
      parametro +=self.contenido.__str__(nivel+1)+ "\n"

      return parametro 

    
class VariablesGlobales:
    def __init__(self,asignacion):
      """Constructor para crear el objeto VariablesGlobales"""
      self.asignacion = asignacion

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo VariablesGlobales"""
      variables_globales = "\t"*nivel+"VariablesGlobales\n"

      variables_globales += self.asignacion.__str__(nivel+1)+ "\n"

      return variables_globales

class AsignarVariable:

    def __init__(self,id_variable,asignacion):
        """Constructor para crear el objeto AsignarVariable"""
        self.id_variable = id_variable
        self.asignacion = asignacion

    def __str__(self,nivel):
        """Metodo para asignar al objeto AsignarVariable"""
        asignar_variable = "\t"*nivel+"AsignarVariable\n"

        asignar_variable +=self.id_variable.__str__(nivel+1)+ "\n"
        asignar_variable +=self.asignacion.__str__(nivel+1)+ "\n"

        return asignar_variable

class ExpresionMatematica:

    def __init__(self,terminos):
      """Constructor para crear el objeto de tipo ExpresionMatematica"""
      self.expresionmatematica = terminos #Puede ser varias opciones de clases

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo ExpresionMatematica"""
      expresionmatematica = "\t"*nivel+"ExpresionMatematica\n"
      for termino in self.expresionmatematica:
        if isinstance(termino, str): #Si es el literal "(" ó ")"
          expresionmatematica += "\t"*(nivel+1) + termino + "\n"
        
        else:             #Si es un objeto de una clase del arbol
          expresionmatematica += termino.__str__(nivel+1)

        return expresionmatematica

class Valor:

    def __init__(self,contenido):
      """Constructor para crear el objeto de tipo Valor"""
      self.contenido = contenido #número entero

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Valor"""
      valor = "\t"*nivel+"Valor"+"\n" + self.contenido.__str__(nivel+1)

      return valor


class Cadena:

    def __init__(self,contenido):
      """Constructor para crear el objeto de tipo Cadena"""
      self.contenido = contenido

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Cadena"""
      cadena = "\t"*nivel+"Cadena - " + self.contenido + "\n"

      return cadena

class Numero:

    def __init__(self,contenido):
      """Constructor para crear el objeto de tipo Numero"""
      self.contenido = contenido

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Numero"""
      numero = "\t"*nivel+"Numero - " + str(self.contenido) + "\n"
      
      return numero



class Retorno:

    def __init__(self,contenido):
      """Constructor para crear el objeto de tipo Retorno"""
      self.contenido = contenido

    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo Retorno"""
      retorno = "\t"*nivel+"Retorno"+ "\n" + self.contenido.__str__(nivel+1) 
      
      return retorno

class FuncionPredefinida:

    def __init__(self,nombrefuncion, parametro1, parametro2):
      """Constructor para crear el objeto de tipo FuncionPredefinida"""
      self.nombrefuncion = nombrefuncion 
      self.parametro1 = parametro1 
      self.parametro2 = parametro2 

    
    def __str__(self,nivel):
      """Metodo para imprimir el nodo del arbol de tipo FuncionPredefinida"""
      funcionpredefinida = "\t"*nivel+"FuncionPredefinida\n"
      funcionpredefinida += "\t"*(nivel+1) + self.nombrefuncion + "\n"
      if self.parametro1 != False:
        funcionpredefinida += self.parametro1.__str__(nivel+1)+ "\n"
      if self.parametro2 != False: 
        funcionpredefinida += self.parametro2.__str__(nivel+1)+ "\n"
      
      return funcionpredefinida

    
class Main:

    def __init__(self,instrucciones):
      """Constructor del main"""
      self.instrucciones = instrucciones

    def __str__(self,nivel):
      main = "\t"*nivel+"Main\n"
        
      for instruccion in self.instrucciones:
        main+= instruccion.__str__(nivel+1)+ "\n"

      return main

