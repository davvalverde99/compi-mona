Proyecto Compiladores e Intérpretes 

Este es el primer proyecto del curso Compiladores e Intérpretes en el Instituto Tecnológico de Costa Rica, impartido por el profesor Aurelio Sanabria Rodríguez durante el segundo semestre del 2020.

El proyecto consiste en desarrollar un compilador para un lenguaje desarrollado por los estudiantes:

- Francisco Mata Blanco
- Jose Daniel Peñaranda Umaña
- David Umaña Blanco
- David Valverde Villalobos
- Joel Vega Godínez

El proyecto esta distribuido de la manera en que los archivos de código se encuentran la carpeta principal, los cuales son micobrujo.py que se encarga de ser la clase principal, scanner.py encargada de clasificar los tokens y sus errores y la clase token que posee los datos de cada token. 

Además, posee una carpeta Pruebas en las que se guardan, ejemplos de errores que aparecen si se digita un token no contemplado por el lenguaje y los tokens obtenidos si se realizó una redacción correcta con la sintaxis del lenguaje.