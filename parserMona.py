import arbol
import token

class Parser:

  def __init__(self,tokens):
      """Constructor de la clase Parser que tiene los tokens obtenidos por el scanner y contadores para avanzar en estos"""
      self.tokens = tokens
      self.token_actual = 0
      self.arbol = None
      self.errores = []

  def returnError(self,esperado):
    """Constructor de la clase Parser que tiene los tokens obtenidos por el scanner y contadores para avanzar en estos"""
    if self.token_actual == len(self.tokens):
        self.token_actual-=1
    self.errores.append("LINEA " + str(self.tokens[self.token_actual].linea) + ". Error en la palabra numero " + str(self.tokens[self.token_actual].palabra))
    if self.token_actual < len(self.tokens):
        self.errores.append("Error: Se esperaba " + esperado + ", pero se recibe " + self.tokens[self.token_actual].valor)

    else:
        self.errores.append("Error: Se esperaba " + esperado + ", pero no se recibe nada")

    return False
  
  def parseMicoBrujo(self):
    """Funcion que se encarga de parsear todas las instrucciones que estan en el archivo .mica"""
    if self.tokens == []:
      return True

    bloque = self.parseBloqueMicoBrujo()

    if self.errores != []: 
       return False

    if (not bloque) and (self.errores == []): 
       return self.returnError("Funcion/VariableGlobal o Main")

    self.arbol = arbol.MicoBrujo(bloque) 
    respuesta = True

    while self.token_actual < len(self.tokens) and respuesta == True:

          bloque = self.parseBloqueMicoBrujo()

          if (not bloque) and (self.errores == []): 
             respuesta = self.returnError("Funcion o VariableGlobal")

          elif (not bloque) and (self.errores != []): 
              respuesta = False

          else:
              self.errores = []
              self.arbol.bloque.append(bloque) 

    return respuesta

  def parseBloqueMicoBrujo(self):
    """Funcion que se encarga de Parsear los bloques de codigo de el .mica"""
    bloque = False
   
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["VariableGlobal"]: 
        bloque = self.parseVariableGlobal()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Funcion","Comentario"]: 
        bloque = self.parseFuncion()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Main"]:
        bloque = self.parseMain()
        
    else:
         return self.returnError("VariableGlobal o Funcion")
        
    return bloque

      

  def parseAsignarVariable(self):
    """Funcion que se encarga de parsear una asignacion de variable"""
    idvariable = False
    asignado = False

    idvariable = self.parseIdentificador()

    if not idvariable:
       return False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "=":
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("el simbolo '='")


    if self.token_actual+1 < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Numero", "Identificador"] and self.tokens[self.token_actual+1].tipo in ["Operador"]:
       asignado = self.parseExpresionMatematica()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Numero", "Cadena"]:
         asignado = self.parseValor()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Identificador"]:
         asignado = self.parseIdentificador()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "FuncionPredefinida":
         asignado = self.parseFuncionPredefinida()

    else:
         return self.returnError("Valor, ExpresionMatematica o FuncionPredefinida")

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ";":
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("el simbolo ';'")

    if not asignado:
       return False

    return arbol.AsignarVariable(idvariable,asignado)

  def parseVariableGlobal(self):
    """Funcion que se encarga de parsear una variable global"""
    asignacion = False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "VariableGlobal": 
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("la palabra sobrada")

    asignacion = self.parseAsignarVariable()

    if not asignacion:
       return False
    
    return arbol.VariablesGlobales(asignacion)


  def parseNumero(self):
    """Funcion que se encarga de parsear un numero"""
    respuesta = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Numero":
       respuesta = arbol.Numero(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1


    else:
        self.returnError("un numero")

    return respuesta

  def parseComentario(self):
    """Funcion que se encarga de parsear un comentario"""
    respuesta = False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Comentario":
       respuesta = arbol.Comentario(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1
      
    return respuesta
  

  def parseCadena(self):
    """Funcion que se encarga de parsear una cadena de texto"""

    respuesta = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Cadena":
       respuesta = arbol.Cadena(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1
       

    else:
        self.returnError("Cadena")

    return respuesta

  def parseIdentificador(self):
    """Funcion que se encarga de parsear un Identificador"""

    respuesta = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Identificador":
       respuesta = arbol.Identificador(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1
  


    else:
        self.returnError("Identificador")

    return respuesta


  def parseValor(self):
    """Funcion que se encarga de parsear un Valor ya sea un Numero o Cadena"""
       
    respuesta = False
    contenido = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Numero":
       contenido = self.parseNumero()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Cadena":
       contenido = self.parseCadena()

    else:
         return self.returnError("Valor")

    if not contenido:
        return False

    respuesta = arbol.Valor(contenido)

    return respuesta

  def parseParametro(self):
    """Funcion que se encarga de parsear un Parametreo, puede ser un Valor o un Identificador"""
       
    respuesta = False
    contenido = False


    if self.token_actual < len(self.tokens) and (self.tokens[self.token_actual].tipo == "Numero" or self.tokens[self.token_actual].tipo == "Cadena"):
       contenido = self.parseValor()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Identificador":
       contenido = self.parseIdentificador()

    else:
         return self.returnError("Parametro")

    if not contenido:
        return False

    respuesta = arbol.Parametro(contenido)

    return respuesta

  def parseOperador(self):
    """Funcion que se encarga de parsear un Operador"""

    respuesta = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Operador":
       respuesta = arbol.Operador(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1


    else:
        return self.returnError("un operador")

    return respuesta

  def parseComparador(self):
    """Funcion que se encarga de parsear un Comparador"""

    respuesta = False


    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Comparador":
       respuesta = arbol.Operador(self.tokens[self.token_actual].valor)
       self.token_actual = self.token_actual + 1



    else:
        return self.returnError("un comparador")

    return respuesta


  def parseFuncionPredefinida(self):
    """Funcion que se encarga de parsear una funcion predefinida de el lenguaje micobrujo"""

    nombrefuncion = False
    parametro1 = False
    parametro2 = False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "indice(":
         nombrefuncion = self.tokens[self.token_actual].valor
         self.token_actual = self.token_actual + 1


         parametro1 = self.parseCadena()

         if not parametro1:
            return False

         if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ",":
            self.token_actual = self.token_actual + 1

         else:
            return self.returnError("el simbolo ','")

         parametro2 = self.parseNumero()

         if not parametro2:
            return False

         if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            self.token_actual = self.token_actual + 1
         else:
              return self.returnError("el simbolo ')'")

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "concatenar(":
         nombrefuncion = self.tokens[self.token_actual].valor
         self.token_actual = self.token_actual + 1

         parametro1 = self.parseCadena()

         if not parametro1:
            return False

         if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ",":
            self.token_actual = self.token_actual + 1
         else:
            return self.returnError("el simbolo ','")

         parametro2 = self.parseCadena()

         if not parametro2:
            return False

         if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            self.token_actual = self.token_actual + 1
         else:
              return self.returnError("el simbolo ')'")

  
    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "largo(":
       nombrefuncion = self.tokens[self.token_actual].valor
       self.token_actual = self.token_actual + 1

       parametro1 = self.parseCadena()

       if not parametro1:
          return False

       if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            self.token_actual = self.token_actual + 1
       else:
            return self.returnError("el simbolo ')'")
          
    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "imprimir(":
       nombrefuncion = self.tokens[self.token_actual].valor
       self.token_actual = self.token_actual + 1

       parametro1 = self.parseCadena()

       if not parametro1:
          return False

       if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            self.token_actual = self.token_actual + 1
       else:
            return self.returnError("el simbolo ')'")
          
    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "convertir(":
       nombrefuncion = self.tokens[self.token_actual].valor
       self.token_actual = self.token_actual + 1


       if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Numero", "Cadena"]:
            parametro1 = self.parseValor()
       else:
            self.returnError("Valor")

       if not parametro1:
          return False

       if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            self.token_actual = self.token_actual + 1

       else:
            return self.returnError("el simbolo ')'")

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "solicitar()":
       nombrefuncion = self.tokens[self.token_actual].valor
       self.token_actual = self.token_actual + 1

    else:
        return self.returnError("el literal de una FuncionPredefinida")


    respuesta = arbol.FuncionPredefinida(nombrefuncion, parametro1, parametro2) 

    return respuesta


  def parseExpresionMatematica(self):
    """Funcion que se encarga de parsear una expresion matematica en una modificacion de variable"""
    respuesta = True
    terminos = []
    cantidad_parentesis = 0

    while self.token_actual < len(self.tokens) and respuesta:

        if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Numero":
            terminos.append(self.parseNumero())

        elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Identificador":
            terminos.append(self.parseIdentificador())

        else:
            return self.returnError("un numero o una variable")

        if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Operador":
            terminos.append(self.parseOperador())
        else:
            return self.returnError("un operador")

        if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Numero":
            terminos.append(self.parseNumero())
            break

        elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Identificador":
            terminos.append(self.parseIdentificador())
            break

        elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "(":
            cantidad_parentesis += 1
            terminos.append(self.tokens[self.token_actual].valor)
            self.token_actual = self.token_actual + 1
        else:
            return self.returnError("un numero, variable o '('")

    while cantidad_parentesis > 0:

        if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
            cantidad_parentesis -= 1
            terminos.append(self.tokens[self.token_actual].valor)
            self.token_actual = self.token_actual + 1

        else:
             return self.returnError("el simbolo ')'")

    respuesta = arbol.ExpresionMatematica(terminos)

    return respuesta

  def parseBifurcacion(self):
    """Funcion que se encarga de parsear una bifurcacion"""

    _if = False
    _else = False

    _if = self.parseSi()

    if not _if:
       return False

    _else = self.parseSino()

    respuesta = arbol.Bifurcacion(_if,_else)

    return respuesta

  def parseComparacion(self):
    """Funcion que se encarga de parsear una comparacion"""
    parametro1 = False
    comparador = False
    parametro2 = False

    if self.token_actual < len(self.tokens) and (self.tokens[self.token_actual].tipo == "Numero" or self.tokens[self.token_actual].tipo == "Cadena" or self.tokens[self.token_actual].tipo == "Identificador"):
          parametro1 = self.parseParametro() 
    else:
         return self.returnError("el parametro")
 
    if not parametro1:
       return False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Comparador":
       comparador = self.parseComparador()
    else:
         return self.returnError("el simbolo del comparador")

    if not comparador:
       return False

    if self.token_actual < len(self.tokens) and (self.tokens[self.token_actual].tipo == "Numero" or self.tokens[self.token_actual].tipo == "Cadena" or self.tokens[self.token_actual].tipo == "Identificador"):
          parametro2 = self.parseParametro() 
    else:
         return self.returnError("el parametro")
        
    if not parametro2:
       return False

    return arbol.Comparacion(parametro1,comparador,parametro2)

  def parseCondicion(self):
    """Funcion que se encarga de parsear una o varias condiciones depedende del caso"""
    condicion1 = False
    condicion2 = False

    condicion1 = self.parseComparacion()

    if not condicion1:
         return False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "OperadorLogico":
       self.token_actual = self.token_actual + 1
       condicion2 = self.parseComparacion()

    return arbol.Condicion(condicion1,condicion2)



  def parseInstruccion(self):
    """Funcion que se encarga de parsear las diferentes instrucciones que contienen los bloques de codigo de .mica"""
    instruccion = False

  
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Si"]:
        instruccion = self.parseBifurcacion()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Repeticion"]:
        instruccion = self.parseRepeticion()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Retorno"]:
      instruccion = self.parseRetorno()

    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Identificador"]:
      instruccion = self.parseAsignarVariable()
      
    else:
      return self.returnError("una Instruccion")

    if not instruccion:
      return False

    return  arbol.Instrucciones(instruccion)



  def parseRetorno(self):
    """Funcion que se encarga de parsear una Variable de retorno ya sea un valor o un identificador"""
    retorno = False
    
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "sarpe":
       self.token_actual = self.token_actual + 1

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Numero","Cadena"]:
       retorno = self.parseValor()
    elif self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Identificador"]:
       retorno = self.parseIdentificador()
    else:
      return self.returnError("Numero, Cadena o Identificador")

    if not retorno:
      return False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["PuntoyComa"]:
       self.token_actual = self.token_actual + 1
    else:
      return self.returnError("el simbolo ';'")


 
    return arbol.Retorno(retorno)



  def parseSi(self):
    """Funcion que se encarga de parsear una condicion si"""

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "diay":
       self.token_actual = self.token_actual + 1

    else:
         return self.returnError("la palabra diay")

    obtenido = self.condicional_contenido_aux() 

    if not obtenido:
        return False

    respuesta = arbol.Si(obtenido[0],obtenido[1])

    return respuesta



  def parseSino(self):
    """Funcion que se encarga de parsear una condicion sino"""
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "ni modo":
       self.token_actual = self.token_actual + 1
    else:
         return False

    obtenido = self.condicional_contenido_aux_2(False,[])

    if not obtenido:
        return False

    respuesta = arbol.Sino(obtenido[1])

    return respuesta

  def condicional_contenido_aux(self):
    """Funcion que se encarga de parsear el contenido de una condicion despues de un si o una repeticion"""
    condicion = False
    contenido = []

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "(":
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("el simbolo '('")
        
    condicion = self.parseCondicion()
 
    if not condicion:
        return False
      
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("el simbolo ')'")

    return self.condicional_contenido_aux_2(condicion,contenido)


  def condicional_contenido_aux_2(self,condicion,contenido):
    """Funcion que se encarga de parsear el contenido de una funcion,bifurcacion o repeticion dentro de los corchetes"""
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "{":
       self.token_actual = self.token_actual + 1
    else:
         return self.returnError("el simbolo '{'")

    haycontenido = False
    while self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor != "}":

          if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo in ["Identificador","Si","Retorno","Repeticion"]:
              inst = self.parseInstruccion()

              if not inst:
                return False
              else:
                contenido.append(inst)
          else:
              return self.returnError("Bifurcacion, Repeticion, Retorno o DeclararVariable")

          haycontenido = True

    if not haycontenido:
       return self.returnError("Procedimiento o DeclararVariable")

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "}":
       self.token_actual = self.token_actual + 1

    else:
         return self.returnError("el simbolo '}'")

    return [condicion, contenido]


  def parseRepeticion(self):
    """Funcion que se encarga de parsear una repeticion"""
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "upee":
       self.token_actual = self.token_actual + 1
 
    else:
         return self.returnError("la palabra upee")

    obtenido = self.condicional_contenido_aux()

    if not obtenido:
        return False

    respuesta = arbol.Repeticion(obtenido[0],obtenido[1])

    return respuesta

  def parseMain(self):
    """Funcion que se encarga de parsear el Main y su contenido"""
    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Main":
       self.token_actual = self.token_actual + 1
    
    else:
         return self.returnError("la palabra mae mona")

    obtenido = self.condicional_contenido_aux_2(False,[])

    if not obtenido:
        return False

    respuesta = arbol.Main(obtenido[1])

    return respuesta


  def parseFuncion(self):
    """Funcion que se encarga de parsear una funcion y su contenido"""
    comentario = False
    parametros = []
    masParametros = False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Comentario": 
       self.token_actual = self.token_actual + 1

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].tipo == "Funcion": 
       self.token_actual = self.token_actual + 1
     
    else:
         return self.returnError("la palabra mae")

    identificador = self.parseIdentificador()

    if not identificador:
        return False

    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == "(":
       self.token_actual = self.token_actual + 1
    
    else:
         return self.returnError("el simbolo '('")

    while self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor != ")" :
      masParametros = False

      if self.tokens[self.token_actual].tipo not in ["Identificador","Cadena","Numero"]:
        break
      parametros.append(self.parseParametro())

      if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ",":
        self.token_actual = self.token_actual + 1
        masParametros = True

    if masParametros:
        return self.returnError("mas parametros")



    if self.token_actual < len(self.tokens) and self.tokens[self.token_actual].valor == ")":
       self.token_actual = self.token_actual + 1
       
    else:
         return self.returnError("el simbolo ')'")

    obtenido = self.condicional_contenido_aux_2(False,[])

    if not obtenido:
        return False

    respuesta = arbol.Funcion(comentario,identificador,parametros,obtenido[1])

    return respuesta




       

    

    


  

  





      

          
        
