import os
import sys
import scanner
import parserMona

#Clase MicoBrujo
def main():
    """La clase Mico Brujo es la clase principal que contiene el main y se encarga
        de llamar al scanner para crear los tokens del archivo de entrada."""

    filename = "Pruebas/" + sys.argv[2]

    if not os.path.exists(filename):
        print("El archivo no existe")
        return 0

    f = open(filename, "r", encoding="utf8")

    line = f.read() #Extrae un string que contiene todos los caracteres del archivo


    print("___  ____            ______            _ ")
    print("|  \/  (_)           | ___ \          (_)    ")
    print("| .  . |_  ___ ___   | |_/ /_ __ _   _ _  ___   ")
    print("| |\/| | |/ __/ _ \  | ___ \ '__| | | | |/ _ \  ")
    print("| |  | | | (_| (_) | | |_/ / |  | |_| | | (_) |  ")
    print("\_|  |_/_|\___\___/  \____/|_|   \__,_| |\___/")
    print("                                     _/ |       ")
    print("                                    |__/        ")
    print("")

    resultadoScanner = scanner.Scanner(line)
    resultadoScanner.scanTokens()

    error = False

    if resultadoScanner.tokenError != []:
        error = True
        for error in resultadoScanner.tokenError:
            print(error)

    if not error:
        #Imprimir tokens
        token = 0
        while token < len(resultadoScanner.tokenLista):
            resultadoScanner.tokenLista[token].printToken()
            token+=1


        resultadoParser = parserMona.Parser(resultadoScanner.tokenLista)
        resultadoParser.parseMicoBrujo()

        #Cuando hay errores en el parseo
        if resultadoParser.errores != []:
            error = True
            for error in resultadoParser.errores:
                print(error)

        #Cuando no hay errores en el parseo
        if not error:
            arbol = resultadoParser.arbol
            print(resultadoParser.arbol) #Imprimir árbol



if __name__== "__main__":
    main()

